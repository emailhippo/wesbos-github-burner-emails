---
title: Email Hippo Wesbos DEA Identification API

language_tabs:
  - shell: cURL
  - http: HTTP
  - javascript: JavaScript
  - java: Java
  - ruby: Ruby
  - javascript--nodejs: Node.JS
  - go: Go

toc_footers: 
  - <a href="https://github.com/wesbos/burner-email-providers" target="_blank">Wesbos Github Repo</a>
  - <a href="https://github.com/wesbos/burner-email-providers" target="_blank">Support and Suggestions</a>
  - <a href="https://app.swaggerhub.com/apis/Email-Hippo/email-hippo_wesbos_dea_identification_api/v1" target="_blank">Swaggerhub &amp; live demo</a>
  - <a href="#ReportNewDEAs">Report new entries</a>
  - <a href="https://www.emailhippo.com/products/more/" target="_blank">Email Hippo</a>

includes: 

search: true

highlight_theme: darkula

headingLevel: 2

---

# Wesbos Burner Email Providers API (v1)

> Scroll down for code samples, example requests and responses. Select a language for code samples from the tabs above or the mobile navigation menu.

This project is a free API wrapper for the burner email providers list created by [wesbos](https://github.com/wesbos).

<a id="ReportNewDEAs"></a>
# Get Involved - Report New Disposable Email Address Domains
This API is only as good as the data provided by The Community. Please get involved in the project by keeping a look out for any new Disposable Domains

If you find a burner email address provider (<abbr title="Also Known As">AKA</abbr> <abbr title="Disposable Email Addresses">DEAs</abbr>) not included on the list, please submit pull requests [here](https://github.com/wesbos/burner-email-providers).


# Possible Use Cases
* Stopping disposable email addresses from signing up to your blog or forum.
* Risk scoring for on-boarding new clients.

# Caveats and Limitations
This service is hosted by [Email Hippo](https://www.emailhippo.com) and is offered as a free service to The Community. As a free service, there are no Service Level Agreements or other guarantees or warranties provided.

## Speed and Throttling
To ensure fair usage and a good performance experience for everyone, there are controls in place to restrict the speed at which you can use this service. The maximum speed permitted is 10 queries per second - after that the service will return HTTP code 429 (slow down) for a duration of 1 minute.

## Coverage and Accuracy
The data returned is derived from the wesbos Github repo and while there are a lot of domains on the list, this is only a small subset of the Disposable Email Address domains known to Email Hippo in it's [Email Verification API](https://www.emailhippo.com/products/more/). That being said, the wesbos list is fairly good and certainly better than having nothing in place to protect your sign up against from abuse from DEAs.

## Support
Officially, this is an unsupported service that is offered on a best efforts basis and can be withdrawn at any time. However, if you have any issues or suggestions, please feel free to raise these on the Github repo and we'll look into it as soon as we are able.

# Integrations

## Client / Server Libraries
There is a comprehensive catalog of client and server code snippets and libraries available over at [SwaggerHub](https://app.swaggerhub.com/apis/Email-Hippo/email-hippo_wesbos_dea_identification_api/v1). 

### Examples of client &amp; server code @ SwaggerHub
* PHP
* ASP.NET core
* Android
* C#
* Spring
* Haskell

To access these, visit [SwaggerHub](https://app.swaggerhub.com/apis/Email-Hippo/email-hippo_wesbos_dea_identification_api/v1) and click the "Export" option on the top right of the page.

# Base URLs

* <a href="https://api.disposable-email-detector.com">https://api.disposable-email-detector.com</a>

<h1 id="Email-Hippo-Wesbos-DEA-Identification-API-DisposableEmails">Disposable Emails API</h1>

Query Disposable Email Address (DEA) records from Wesbos [Github repo](https://github.com/wesbos/burner-email-providers).

# V1 API

<a id="opIdApiDeaV1CheckByQueryGet"></a>

> Code samples

```shell
# You can also use wget
curl -X GET https://api.disposable-email-detector.com/api/dea/v1/check/{query} \
  -H 'Accept: application/json'

```

```http
GET https://api.disposable-email-detector.com/api/dea/v1/check/{query} HTTP/1.1

Accept: application/json

```

```javascript
var headers = {
  'Accept':'application/json'

};

$.ajax({
  url: 'https://api.disposable-email-detector.com/api/dea/v1/check/{query}',
  method: 'get',

  headers: headers,
  success: function(data) {
    console.log(JSON.stringify(data));
  }
})

```

```java
URL obj = new URL("https://api.disposable-email-detector.com/api/dea/v1/check/{query}");
HttpURLConnection con = (HttpURLConnection) obj.openConnection();
con.setRequestMethod("GET");
int responseCode = con.getResponseCode();
BufferedReader in = new BufferedReader(
    new InputStreamReader(con.getInputStream()));
String inputLine;
StringBuffer response = new StringBuffer();
while ((inputLine = in.readLine()) != null) {
    response.append(inputLine);
}
in.close();
System.out.println(response.toString());

```

```ruby
require 'rest-client'
require 'json'

headers = {
  'Accept' => 'application/json'
}

result = RestClient.get 'https://api.disposable-email-detector.com/api/dea/v1/check/{query}',
  params: {
  }, headers: headers

p JSON.parse(result)

```

```javascript--nodejs
const fetch = require('node-fetch');

const headers = {
  'Accept':'application/json'

};

fetch('/api.disposable-email-detector.com/api/dea/v1/check/{query}',
{
  method: 'GET',

  headers: headers
})
.then(function(res) {
    return res.json();
}).then(function(body) {
    console.log(body);
});

```

```go
package main

import (
       "bytes"
       "net/http"
)

func main() {

    headers := map[string][]string{
        "Accept": []string{"application/json"},
        
    }

    data := bytes.NewBuffer([]byte{jsonReq})
    req, err := http.NewRequest("GET", "https://api.disposable-email-detector.com/api/dea/v1/check/{query}", data)
    req.Header = headers

    client := &http.Client{}
    resp, err := client.Do(req)
    // ...
}

```

`GET /api/dea/v1/check/{query}`

*Check if an email address or domain is a Disposable Email Address (DEA).*

Lookup a domain or email address against the Wesbos Github DEA list.

<h3 id="apideav1checkbyqueryget-parameters">Parameters</h3>

|Parameter|In|Type|Required|Description|
|---|---|---|---|---|
|query|path|string|true|The domain or email address to query.|

> Example responses

> 200 Response

```json
{
  "doc": "string",
  "query": "string",
  "result": {
    "isDisposable": true,
    "error": "string"
  }
}
```

<h3 id="apideav1checkbyqueryget-responses">Responses</h3>

|Status|Meaning|Description|Schema|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|Success|[DisposableDomainResult](#schemadisposabledomainresult)|
|400|[Bad Request](https://tools.ietf.org/html/rfc7231#section-6.5.1)|Bad request. Request is empty/null or exceeds the maximum allowed length of 255 characters.|None|
|429|[Too Many Requests](https://tools.ietf.org/html/rfc6585#section-4)|Maximum processing rate exceeded. Please slow your requests to &lt; 10 queries per second.|None|
|500|[Internal Server Error](https://tools.ietf.org/html/rfc7231#section-6.6.1)|Server error.|None|

<aside class="success">
This operation does not require authentication
</aside>

# Schemas

<h2 id="tocSdisposabledomainresult">DisposableDomainResult</h2>

<a id="schemadisposabledomainresult"></a>

```json
{
  "doc": "string",
  "query": "string",
  "result": {
    "isDisposable": true,
    "error": "string"
  }
}

```

*The result that shows whether the domain is disposable together with any error information.*

### Properties

|Name|Type|Required|Restrictions|Description|
|---|---|---|---|---|
|doc|string|false|none|The documentation URL.|
|query|string|false|none|The query (i.e the domain name to check against the main wesbos list).|
|result|[DisposableDomainMessage](#schemadisposabledomainmessage)|false|none|Disposable Domain Message|

<h2 id="tocSdisposabledomainmessage">DisposableDomainMessage</h2>

<a id="schemadisposabledomainmessage"></a>

```json
{
  "isDisposable": true,
  "error": "string"
}

```

*Disposable Domain Message*

### Properties

|Name|Type|Required|Restrictions|Description|
|---|---|---|---|---|
|isDisposable|boolean|false|none|Whether this domain is a disposable domain.|
|error|string|false|none|If there is an error, the message is described here.|